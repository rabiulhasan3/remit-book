

## Remit-Book


Remit-Book is an Banking Remittance Management System Using Laravel.


## Package Uses



## Installation Instruction

- Clone the repo.
- Run `composer install`
- Run `copy .env.example .env`
- Run `php artisan migrate --seed`



## Contribution Guideline

- Fork the repo.
- Clone the repo locally
- Run `git checkout dev`
- Create a new local branch
- Work on your local branch
- Push to remote
- When work is tested, done or ready, push to remote
- Marge to dev


## License

The Remit-book software licensed under the [MIT license](https://opensource.org/licenses/MIT).